const { User } = require("../models");
const {
  hashPassword,
  comparePassword
} = require("../utils/helpers/passwordHandler");
const {
  createAccessToken,
  createRefreshToken,
  decodedToken
} = require("../utils/helpers/jwtHandler");
const { existEmail } = require("../utils/helpers/userHandler");
const boom = require("@hapi/boom");
const moment = require("moment");

async function signUp({ user }) {
  user.email = user.email.toLowerCase();
  user.password = hashPassword(user.password);

  if (await existEmail(user.email)) {
    return {
      error: true,
      boom: boom.badRequest("invalid email or password")
    };
  }

  const userCreated = new User(user);
  await userCreated.save();

  return {
    error: false,
    data: userCreated._id
  };
}

async function signIn({ email, password }) {
  const user = await User.findOne({ email: email.toLowerCase() });
  const result = validationSignIn(user, password);
  if (result) {
    return result;
  }

  return {
    error: false,
    data: {
      accessToken: createAccessToken(user),
      refreshToken: createRefreshToken(user)
    }
  };
}

function validationSignIn(user, password) {
  if (!user) {
    return {
      error: true,
      boom: boom.unauthorized("invalid email or password")
    };
  }

  const isEquals = comparePassword(user.password, password);
  if (!isEquals) {
    return {
      error: true,
      boom: boom.unauthorized("invalid email or password")
    };
  }

  if (!user.active) {
    return {
      error: true,
      boom: boom.unauthorized(
        "to log in, activate your account by replying to the message that was sent to your email"
      )
    };
  }
  return false;
}

function willExpireToken(token) {
  const { exp } = decodedToken(token);
  const currentDate = moment().unix();
  return currentDate > exp ? true : false;
}

async function refreshToken({ refreshToken }) {
  if (willExpireToken(refreshToken)) {
    return { error: true, boom: boom.unauthorized("refreshToken expires") };
  }

  const { id } = decodedToken(refreshToken);
  const user = await User.findById(id);

  if (!user) {
    return { error: true, boom: boom.unauthorized() };
  }

  return {
    error: false,
    data: {
      accessToken: createAccessToken(user),
      refreshToken
    }
  };
}

module.exports = {
  signUp,
  signIn,
  refreshToken
};
