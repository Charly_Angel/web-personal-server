const { Post } = require("../models");
const slugGenerator = require("slug");
const boom = require("@hapi/boom");

async function addPost({ post }) {
  const slug = slugGenerator(post.title);
  if (await existSlugPost(slug)) {
    return {
      error: true,
      boom: boom.badRequest("the title of the post already exists ")
    };
  }
  post.slug = slug;
  const postCreated = new Post(post);
  await postCreated.save();

  if (!postCreated._id) {
    return { error: true, boom: boom.badRequest("post not created") };
  }

  return {
    error: false,
    data: postCreated._id
  };
}

async function existSlugPost(slug, id = false) {
  const post = await Post.findOne({ slug });
  if (id && post) {
    return String(post._id) === String(id) ? false : true;
  }
  return post ? true : false;
}

async function getPosts({ page = 1, limit = 10, active = true }) {
  const options = {
    page,
    limit: parseInt(limit),
    sort: { date: "desc" }
  };
  const posts = await Post.paginate({ active }, options);

  return {
    error: false,
    data: posts ? posts : []
  };
}

async function updatePost({ id, post }) {
  if (post.title) {
    const slug = slugGenerator(post.title);
    if (await existSlugPost(slug, id)) {
      return {
        error: true,
        boom: boom.badRequest("the title of the post already exists ")
      };
    }
    post.slug = slug;
  }
  const postUpdated = await Post.findByIdAndUpdate(id, post);

  if (!postUpdated._id) {
    return { error: true, boom: boom.badRequest("post not found") };
  }

  return {
    error: false,
    data: postUpdated._id
  };
}

async function getPost({ slug }) {
  const post = await Post.findOne({ slug });

  if (!post) {
    return { error: true, boom: boom.badRequest("post not found") };
  }

  return {
    error: false,
    data: post
  };
}

async function deletePost(id) {
  const postDeleted = await Post.findByIdAndDelete(id);

  if (!postDeleted) {
    return { error: true, boom: boom.notFound("post not found") };
  }

  return {
    error: false,
    data: postDeleted._id
  };
}

async function getAllPosts({ page = 1, limit = 10 }) {
  const options = {
    page,
    limit: parseInt(limit),
    sort: { date: "desc" }
  };
  const posts = await Post.paginate({}, options);

  return {
    error: false,
    data: posts ? posts : []
  };
}

module.exports = {
  addPost,
  getPosts,
  updatePost,
  getPost,
  deletePost,
  getAllPosts
};
