const { User } = require('../models');
const { validateImage, pathImage } = require('../utils/helpers/uploadFile');
const boom = require('@hapi/boom');
const { hashPassword } = require('../utils/helpers/passwordHandler');
const { existEmail } = require('../utils/helpers/userHandler');

async function getUsers() {
  const users = await User.find();

  return {
    error: false,
    data: users ? users : [],
  };
}

async function getUsersActive({ active }) {
  const users = await User.find({ active });

  return {
    error: false,
    data: users ? users : [],
  };
}

async function uploadAvatar({ id, avatar }) {
  const fileName = validateImage(avatar);
  if (!fileName) {
    return {
      error: true,
      boom: boom.badRequest(
        'Only images with jpg, jpeg and png extensions are allowed.',
      ),
    };
  }

  const user = await User.findByIdAndUpdate({ _id: id }, { avatar: fileName });
  if (!user) {
    return { error: true, boom: boom.notFound() };
  }

  return {
    error: false,
    data: fileName,
  };
}

async function getAvatar({ avatar }) {
  const path = pathImage('avatar', avatar);
  if (!path) {
    return { error: true, boom: boom.notFound('avatar not found') };
  }

  return {
    error: false,
    data: path,
  };
}

async function updateUser({ id, user }) {
  if (user.password) {
    /**hashed password */
    user.password = hashPassword(user.password);
  } else {
    delete user.password;
  }

  user.email = user.email.toLowerCase();
  const userUpdated = await User.findByIdAndUpdate({ _id: id }, user, {
    new: true,
  });
  if (!userUpdated) {
    return { error: true, boom: boom.notFound('user not found') };
  }

  return {
    error: false,
    data: userUpdated._id,
  };
}

async function activateUser({ active, id }) {
  const userUpdated = await User.findByIdAndUpdate(id, { active });
  if (!userUpdated) {
    return { error: true, boom: boom.notFound('user not found') };
  }
  return {
    error: false,
    data: 'ok',
  };
}

async function deleteUser({ id }) {
  const userDeleted = await User.findByIdAndDelete(id);
  if (!userDeleted) {
    return { error: true, boom: boom.notFound('user not found') };
  }
  return {
    error: false,
    data: userDeleted._id,
  };
}

async function signUpAdmin({ user }) {
  user.email = user.email.toLowerCase();
  user.password = hashPassword(user.password);
  user.active = true;

  if (await existEmail(user.email)) {
    return {
      error: true,
      boom: boom.badRequest('invalid email or password'),
    };
  }

  const userCreated = new User(user);
  await userCreated.save();

  return {
    error: false,
    data: userCreated._id,
  };
}

module.exports = {
  getUsers,
  getUsersActive,
  uploadAvatar,
  getAvatar,
  updateUser,
  activateUser,
  deleteUser,
  signUpAdmin,
};
