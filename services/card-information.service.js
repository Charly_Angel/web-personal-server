const { CardInformation } = require('../models');
const boom = require('@hapi/boom');

async function addCardInformation({ cardInformation }) {
  const cardInformationCreated = new CardInformation(cardInformation);
  await cardInformationCreated.save();

  if (!cardInformationCreated._id) {
    return { error: true, boom: boom.badRequest() };
  }

  return {
    error: false,
    data: cardInformationCreated._id,
  };
}

async function getTypeCardsInformation() {
  const cardsInformation = await CardInformation.find();

  return {
    error: false,
    data: cardsInformation ? cardsInformation : [],
  };
}

async function updateCardInformation({ id, cardInformation }) {
  const cardInformationUpdated = await CardInformation.findByIdAndUpdate(
    id,
    cardInformation,
  );

  if (!cardInformationUpdated._id) {
    return { error: true, boom: boom.notFound('card information not found') };
  }

  return {
    error: false,
    data: cardInformationUpdated._id,
  };
}

async function deleteCardInformation(id) {
  const cardInformationDeleted = await CardInformation.findByIdAndDelete(id);

  if (!cardInformationDeleted) {
    return { error: true, boom: boom.notFound('card information not found') };
  }

  return {
    error: false,
    data: cardInformationDeleted._id,
  };
}

module.exports = {
  addCardInformation,
  getTypeCardsInformation,
  updateCardInformation,
  deleteCardInformation,
};
