const express = require('express');

const { AuthController } = require('../controllers');
const { UserSchema } = require('../utils/schemas');
const validation = require('../utils/middleware/validationHandler');

function authApp(app) {
  const router = express.Router();

  app.use('/api/v1/auth', router);

  /**
   * @openapi
   * /api/v1/auth/sign-up:
   *   post:
   *     tags:
   *       - auth
   *     summary: crear un nuevo usuario
   *     requestBody:
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/createUserDto'
   *         required: true
   *     responses:
   *       201:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: user created
   */
  router.post('/sign-up', validation(UserSchema.createUserSchema), AuthController.signUp);

  /**
   * @openapi
   * /api/v1/auth/sign-in:
   *   post:
   *     tags:
   *       - auth
   *     summary: iniciar sesión
   *     requestBody:
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/loginDto'
   *         required: true
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: object
   *                   properties:
   *                     accessToken:
   *                       type: string
   *                     refreshToken:
   *                       type: string
   *                 message:
   *                   type: string
   *                   example: user logged
   */
  router.post('/sign-in', validation(UserSchema.signInSchema), AuthController.signIn);

  /**
   * @openapi
   * /api/v1/auth/refresh-access-token:
   *   post:
   *     tags:
   *       - auth
   *     summary: refrescar el token del usuario
   *     requestBody:
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 refreshToken:
   *                   type: string
   *         required: true
   *     responses:
   *       201:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: object
   *                   properties:
   *                     accessToken:
   *                       type: string
   *                     refreshToken:
   *                       type: string
   *                 message:
   *                   type: string
   *                   example: token refreshed
   */
  router.post('/refresh-access-token', AuthController.refreshToken);
}

module.exports = authApp;
