const express = require('express');

const { PostController } = require('../controllers');
const md_auth = require('../utils/middleware/authenticated');
const { PostSchema, MongoSchema } = require('../utils/schemas');
const validation = require('../utils/middleware/validationHandler');

function postApp(app) {
  const router = express.Router();
  app.use('/api/v1/post', router);

  /**
   * @openapi
   * /api/v1/post/new:
   *   post:
   *     tags:
   *       - post
   *     summary: crear un post
   *     requestBody:
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/createPostDto'
   *         required: true
   *     responses:
   *       201:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: post registered
   */
  router.post(
    '/new',
    [md_auth.ensureAuth, validation(PostSchema.createPostSchema)],
    PostController.addPost,
  );

  /**
   * @openapi
   * /api/v1/post:
   *   get:
   *     tags:
   *       - post
   *     summary: obtener los posts activos
   *     parameters:
   *        - name: limit
   *          in: query
   *          description: cantidad de post por página
   *          required: false
   *          schema:
   *            type: string
   *        - name: active
   *          in: query
   *          description: filtrar por post activos/inactivos
   *          required: false
   *          schema:
   *            type: boolean
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: array
   *                   items:
   *                     $ref: '#/components/schemas/postDto'
   *                 message:
   *                   type: string
   *                   example: post listed
   */
  router.get('/', PostController.getPosts);

  /**
   * @openapi
   * /api/v1/post/all:
   *   get:
   *     tags:
   *       - post
   *     summary: obtener los posts
   *     parameters:
   *        - name: limit
   *          in: query
   *          description: cantidad de post por página
   *          required: false
   *          schema:
   *            type: string
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: array
   *                   items:
   *                     $ref: '#/components/schemas/postDto'
   *                 message:
   *                   type: string
   *                   example: post listed
   */
  router.get('/all', md_auth.ensureAuth, PostController.getAllPosts);

  /**
   * @openapi
   * /api/v1/post/{id}/update:
   *   put:
   *     tags:
   *       - post
   *     summary: actualizar un post
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID del post
   *          required: true
   *          schema:
   *            type: string
   *     requestBody:
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/updatePostDto'
   *         required: true
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: post updated
   */
  router.put(
    '/:id/update',
    [
      md_auth.ensureAuth,
      validation(MongoSchema.mongoIdSchema, 'params'),
      validation(PostSchema.updatePostSchema),
    ],
    PostController.updatePost,
  );

  /**
   * @openapi
   * /api/v1/post/{slug}:
   *   get:
   *     tags:
   *       - post
   *     summary: obtener un post
   *     parameters:
   *        - name: slug
   *          in: path
   *          description: slug del post
   *          required: true
   *          schema:
   *            type: string
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: array
   *                   items:
   *                     $ref: '#/components/schemas/postDto'
   *                 message:
   *                   type: string
   *                   example: post found
   */
  router.get('/:slug', PostController.getPost);

  /**
   * @openapi
   * /api/v1/post/{id}/delete:
   *   delete:
   *     tags:
   *       - post
   *     summary: eliminar un post
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID del post
   *          required: true
   *          schema:
   *            type: string
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: post deleted
   */
  router.delete(
    '/:id/delete',
    validation(MongoSchema.mongoIdSchema, 'params'),
    PostController.deletePost,
  );

  /**
   * @openapi
   * /api/v1/post/{id}/publish:
   *   put:
   *     tags:
   *       - post
   *     summary: publicar un post
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID del post
   *          required: true
   *          schema:
   *            type: string
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: post published
   */
  router.put(
    '/:id/publish',
    [
      md_auth.ensureAuth,
      validation(MongoSchema.mongoIdSchema, 'params'),
      validation(PostSchema.updatePostSchema),
    ],
    PostController.publishPost,
  );
}

module.exports = postApp;
