const express = require('express');
const multipart = require('connect-multiparty');

const { UserController } = require('../controllers');
const md_auth = require('../utils/middleware/authenticated');
const md_upload_avatar = multipart({ uploadDir: './uploads/avatar' });
const { UserSchema } = require('../utils/schemas');
const validation = require('../utils/middleware/validationHandler');

function userApp(app) {
  const router = express.Router();
  app.use('/api/v1/users', router);

  /**
   * @openapi
   * /api/v1/users:
   *   get:
   *     tags:
   *       - users
   *     summary: obtener los usuarios
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: array
   *                   items:
   *                     $ref: '#/components/schemas/userDto'
   *                 message:
   *                   type: string
   *                   example: user listed
   */
  router.get('/', [md_auth.ensureAuth], UserController.getUsers);

  /**
   * @openapi
   * /api/v1/users/active:
   *   get:
   *     tags:
   *       - users
   *     summary: obtener los usuarios activos o inactivos
   *     parameters:
   *        - name: active
   *          in: query
   *          description: propiedad que indica si el usuario esta activo
   *          required: false
   *          schema:
   *            type: boolean
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: array
   *                   items:
   *                     $ref: '#/components/schemas/userDto'
   *                 message:
   *                   type: string
   *                   example: user active listed/user in active listed
   */
  router.get('/active', [md_auth.ensureAuth], UserController.getUsersActive);

  /**
   * @openapi
   * /api/v1/users/{id}/upload-avatar:
   *   put:
   *     tags:
   *       - users
   *     summary: subir el avatar de un usuario
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID del usuario
   *          required: true
   *          schema:
   *            type: string
   *     requestBody:
   *         content:
   *           application/octet-stream:
   *             schema:
   *               type: string
   *               format: binary
   *         required: true
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: I84SpIFoMLwsOoYi15kj3kuo.png
   *                 message:
   *                   type: string
   *                   example: upload avatar success
   */
  router.put(
    '/:id/upload-avatar',
    [md_auth.ensureAuth, md_upload_avatar],
    UserController.uploadAvatar,
  );

  /**
   * @openapi
   * /api/v1/users/{avatar}/avatar:
   *   get:
   *     tags:
   *       - users
   *     summary: obtener el avatar de un usuario
   *     parameters:
   *        - name: image
   *          in: path
   *          description: nombre del avatar
   *          required: true
   *          schema:
   *            type: string
   *            example: I84SpIFoMLwsOoYi15kj3kuo.png
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/octet-stream:
   *             schema:
   *               type: string
   *               format: binary
   */
  router.get('/:avatar/avatar', UserController.getAvatar);

  /**
   * @openapi
   * /api/v1/users/{id}/update:
   *   put:
   *     tags:
   *       - users
   *     summary: actualizar un usuario
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID del usuario
   *          required: true
   *          schema:
   *            type: string
   *     requestBody:
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/updateUserDto'
   *         required: true
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: user updated
   */
  router.put(
    '/:id/update',
    [md_auth.ensureAuth, validation(UserSchema.updateSchema)],
    UserController.updateUser,
  );

  /**
   * @openapi
   * /api/v1/users/{id}/activate:
   *   put:
   *     tags:
   *       - users
   *     summary: activar o desactivar un usuario
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID del usuario
   *          required: true
   *          schema:
   *            type: string
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: user successfully activated/disabled
   */
  router.put(
    '/:id/activate',
    [md_auth.ensureAuth, validation(UserSchema.activeUserSchema)],
    UserController.activateUser,
  );

  /**
   * @openapi
   * /api/v1/users/{id}/delete:
   *   delete:
   *     tags:
   *       - users
   *     summary: eliminar un usuario
   *     parameters:
   *        - name: id
   *          in: path
   *          description: ID del usuario
   *          required: true
   *          schema:
   *            type: string
   *     responses:
   *       200:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: user deleted
   */
  router.delete('/:id/delete', [md_auth.ensureAuth], UserController.deleteUser);

  /**
   * @openapi
   * /api/v1/users/new:
   *   post:
   *     tags:
   *       - users
   *     summary: crear un usuario
   *     requestBody:
   *         content:
   *           application/json:
   *             schema:
   *               $ref: '#/components/schemas/createUserDto'
   *         required: true
   *     responses:
   *       201:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: user registered
   */
  router.post(
    '/new',
    [md_auth.ensureAuth, validation(UserSchema.createAdminSchema)],
    UserController.signUpAdmin,
  );
}

module.exports = userApp;
