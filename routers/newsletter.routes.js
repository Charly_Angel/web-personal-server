const express = require('express');

const { NewsletterController } = require('../controllers');
const { NewsletterSchema } = require('../utils/schemas');
const validation = require('../utils/middleware/validationHandler');

function newsletterApp(app) {
  const router = express.Router();
  app.use('/api/v1/newsletter', router);

  /**
   * @openapi
   * /api/v1/newsletter/{email}/suscribe:
   *   post:
   *     tags:
   *       - newsletter
   *     summary: registrar un email al newsletter
   *     parameters:
   *        - name: email
   *          in: path
   *          description: email para registrar
   *          required: true
   *          schema:
   *            type: string
   *     responses:
   *       201:
   *         description: OK
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 error:
   *                   type: boolean
   *                   example: false
   *                 data:
   *                   type: string
   *                   example: 63992533f0eba0f524033e21
   *                 message:
   *                   type: string
   *                   example: email registered
   */
  router.post(
    '/:email/suscribe',
    validation({ email: NewsletterSchema.suscribeEmailNewsletterSchema }, 'params'),
    NewsletterController.suscribeEmail,
  );
}

module.exports = newsletterApp;
