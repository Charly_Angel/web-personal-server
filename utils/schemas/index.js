module.exports = {
  MongoSchema: require('./mongo.schema'),
  UserSchema: require('./users.schema'),
  MenuSchema: require('./menu.schema'),
  NewsletterSchema: require('./newsletter.schema'),
  CourseSchema: require('./course.schema'),
  PostSchema: require('./post.schema'),
  CardInformationSchema: require('./card-information.schema'),
  ReviewSchema: require('./review.schema'),
};
