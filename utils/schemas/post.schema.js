const joi = require('joi');

const IdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const postTitleSchema = joi.string();
const postDescriptionSchema = joi.string();
const postActiveSchema = joi.boolean();
const postDateSchema = joi.date();

/**
 * @openapi
 * components:
 *   schemas:
 *     postDto:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *         title:
 *           type: string
 *         description:
 *           type: string
 *         active:
 *           type: boolean
 *         date:
 *           type: number
 *           example: 1670185967259
 *     createPostDto:
 *       type: object
 *       properties:
 *         title:
 *           type: string
 *         description:
 *           type: string
 *         date:
 *           type: number
 *           example: 1670185967259
 *     updatePostDto:
 *       type: object
 *       properties:
 *         title:
 *           type: string
 *         description:
 *           type: string
 *         active:
 *           type: boolean
 *         date:
 *           type: number
 *           example: 1670185967259
 */
const createPostSchema = {
  title: postTitleSchema.required(),
  description: postDescriptionSchema.required(),
  date: postDateSchema,
};

const updatePostSchema = {
  title: postTitleSchema,
  description: postDescriptionSchema,
  date: postDateSchema,
  active: postActiveSchema,
};

module.exports = {
  createPostSchema,
  updatePostSchema,
};
