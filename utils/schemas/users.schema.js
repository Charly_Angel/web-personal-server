const joi = require('joi');

const idSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const nameSchema = joi.string().max(100);
const lastNameSchema = joi.string().max(100);
const emailSchema = joi.string().email();
const passwordSchema = joi.string().min(8);
const roleSchema = joi.string();
const activeSchema = joi.boolean();

/**
 * @openapi
 * components:
 *   schemas:
 *     createUserDto:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *         lastname:
 *           type: string
 *         email:
 *           type: string
 *         password:
 *           type: string
 *     updateUserDto:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *         lastname:
 *           type: string
 *         email:
 *           type: string
 *         password:
 *           type: string
 *         role:
 *           type: boolean
 *     loginDto:
 *       type: object
 *       properties:
 *         email:
 *           type: string
 *         password:
 *           type: string
 *     userDto:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *         name:
 *           type: string
 *         lastname:
 *           type: string
 *         email:
 *           type: string
 *         avatar:
 *           type: string
 *         active:
 *           type: boolean
 *         role:
 *           type: boolean
 */
const userSchema = {
  name: nameSchema.required(),
  lastname: lastNameSchema.required(),
  email: emailSchema.required(),
  password: passwordSchema.required(),
};

const createSchema = {
  ...userSchema,
};

const createAdminSchema = {
  ...userSchema,
  role: roleSchema.required(),
};

const updateSchema = {
  name: nameSchema,
  lastname: lastNameSchema,
  email: emailSchema,
  password: passwordSchema,
  role: roleSchema,
};

const signInSchema = {
  email: emailSchema.required(),
  password: passwordSchema.required(),
};

const activeUserSchema = {
  active: activeSchema.required(),
};

module.exports = {
  idSchema,
  createSchema,
  updateSchema,
  signInSchema,
  activeUserSchema,
  createAdminSchema,
};
