const joi = require("joi");

const emailSchema = joi.string().email();

const suscribeEmailNewsletterSchema = emailSchema.required();

module.exports = {
  suscribeEmailNewsletterSchema
};
