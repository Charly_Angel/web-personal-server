const joi = require('joi');

const IdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);

const TitleSchema = joi.string();
const DescriptionSchema = joi.string();
const TypeSchema = joi.string();
const OfferSchema = joi.boolean();
const LinkSchema = joi.string().uri();
const CouponSchema = joi.string().empty('');
const FreeSchema = joi.boolean();
const limitSchema = joi.number().positive();

/**
 * @openapi
 * components:
 *   schemas:
 *     courseDto:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *         title:
 *           type: string
 *         description:
 *           type: string
 *         type:
 *           type: string
 *         offer:
 *           type: boolean
 *         link:
 *           type: string
 *         image:
 *           type: string
 *         coupon:
 *           type: string
 *         free:
 *           type: boolean
 *     createCourseDto:
 *       type: object
 *       properties:
 *         title:
 *           type: string
 *         description:
 *           type: string
 *         type:
 *           type: string
 *         offer:
 *           type: boolean
 *         link:
 *           type: string
 *         coupon:
 *           type: string
 *         free:
 *           type: boolean
 *     updateCourseDto:
 *       type: object
 *       properties:
 *         title:
 *           type: string
 *         description:
 *           type: string
 *         type:
 *           type: string
 *         offer:
 *           type: boolean
 *         link:
 *           type: string
 *         coupon:
 *           type: string
 *         free:
 *           type: boolean
 */

const create = {
  title: TitleSchema.required(),
  description: DescriptionSchema.required(),
  type: TypeSchema.required(),
  offer: OfferSchema,
  link: LinkSchema.required(),
  coupon: CouponSchema,
  free: FreeSchema,
};

const update = {
  title: TitleSchema,
  description: DescriptionSchema,
  type: TypeSchema,
  offer: OfferSchema,
  link: LinkSchema,
  coupon: CouponSchema,
  free: FreeSchema,
};

const limit = {
  limit: limitSchema,
};

module.exports = {
  create,
  update,
  limit,
};
