const joi = require('joi');

const IdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const titleSchema = joi.string();
const descriptionSchema = joi.string();
const iconSchema = joi.string();

/**
 * @openapi
 * components:
 *   schemas:
 *     cardInformationDto:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *         title:
 *           type: string
 *         description:
 *           type: string
 *         icon:
 *           type: string
 *     createCardInformationDto:
 *       type: object
 *       properties:
 *         title:
 *           type: string
 *         description:
 *           type: string
 *         icon:
 *           type: string
 *     updateCardInformationDto:
 *       type: object
 *       properties:
 *         title:
 *           type: string
 *         description:
 *           type: string
 *         icon:
 *           type: string
 */

const create = {
  title: titleSchema.required(),
  description: descriptionSchema.required(),
  icon: iconSchema.required(),
};

const update = {
  title: titleSchema,
  description: descriptionSchema,
  icon: iconSchema,
};

const id = {
  id: IdSchema.required(),
};

module.exports = {
  id,
  create,
  update,
};
