const { port } = require("./config");
const connectionDB = require("./lib/mongo");
const debug = require("debug")("app:server");
const app = require("./app");

/** server */
app.listen(port, async () => {
  await connectionDB();
  debug(`Listening http://localhost:${port}`);
});
