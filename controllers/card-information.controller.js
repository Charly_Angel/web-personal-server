const { CardInformationService } = require('../services');
const { success, error } = require('../network/response');

async function addCardInformation(req, res, next) {
  const cardInformation = req.body;
  try {
    const result = await CardInformationService.addCardInformation({
      cardInformation,
    });
    !result.error
      ? success(req, res, result.data, 'card created', 201)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function getTypeCardsInformation(req, res, next) {
  try {
    const result = await CardInformationService.getTypeCardsInformation();
    !result.error
      ? success(req, res, result.data, 'cards listed', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function updateCardInformation(req, res, next) {
  const cardInformation = req.body;
  const { id } = req.params;
  try {
    const result = await CardInformationService.updateCardInformation({
      cardInformation,
      id,
    });
    !result.error
      ? success(req, res, result.data, 'card updated', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function deleteCardInformation(req, res, next) {
  const { id } = req.params;
  try {
    const result = await CardInformationService.deleteCardInformation(id);
    !result.error
      ? success(req, res, result.data, 'card deleted', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

module.exports = {
  addCardInformation,
  getTypeCardsInformation,
  updateCardInformation,
  deleteCardInformation,
};
