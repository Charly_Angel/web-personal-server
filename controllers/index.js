module.exports = {
  AuthController: require('./auth.controller'),
  UserController: require('./user.controller'),
  MenuController: require('./menu.controller'),
  NewsletterController: require('./newslatter.controller'),
  CourseController: require('./course.controller'),
  PostController: require('./post.controller'),
  CardInformationController: require('./card-information.controller'),
  ReviewController: require('./review.controller'),
};
