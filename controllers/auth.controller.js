const { AuthService } = require("../services");
const { success, error } = require("../network/response");

async function signUp(req, res, next) {
  const { body: user } = req;
  try {
    const result = await AuthService.signUp({ user });
    !result.error
      ? success(req, res, result.data, "user created", 201)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}
async function signIn(req, res, next) {
  const { email, password } = req.body;
  try {
    const result = await AuthService.signIn({ email, password });
    !result.error
      ? success(req, res, result.data, "user logged", 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function refreshToken(req, res, next) {
  const { refreshToken } = req.body;
  try {
    const result = await AuthService.refreshToken({ refreshToken });
    !result.error
      ? success(req, res, result.data, "token refreshed", 201)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

module.exports = {
  signUp,
  signIn,
  refreshToken
};
