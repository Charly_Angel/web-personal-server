const { ReviewService } = require('../services');
const { success, error, file } = require('../network/response');

async function create(req, res, next) {
  const review = req.body;
  try {
    const result = await ReviewService.create({
      review,
    });
    !result.error
      ? success(req, res, result.data, 'review created', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function getAll(req, res, next) {
  try {
    const result = await ReviewService.getAll();
    !result.error
      ? success(req, res, result.data, 'review listed', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function update(req, res, next) {
  const review = req.body;
  const { id } = req.params;
  try {
    const result = await ReviewService.update({
      review,
      id,
    });
    !result.error
      ? success(req, res, result.data, 'review updated', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function remove(req, res, next) {
  const { id } = req.params;
  try {
    const result = await ReviewService.remove(id);
    !result.error
      ? success(req, res, result.data, 'review deleted', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function uploadImage(req, res, next) {
  const { id } = req.params;
  const image = req.files.image;
  try {
    const result = await ReviewService.uploadImage({ id, image });
    !result.error
      ? success(req, res, result.data, 'upload image review success', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function getImage(req, res, next) {
  const { image } = req.params;
  try {
    const result = await ReviewService.getImage({ image });
    !result.error
      ? file(req, res, result.data, 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

module.exports = {
  create,
  getAll,
  update,
  remove,
  uploadImage,
  getImage,
};
