const { PostService } = require('../services');

const { success, error } = require('../network/response');

async function addPost(req, res, next) {
  const post = req.body;
  try {
    const result = await PostService.addPost({ post });
    !result.error
      ? success(req, res, result.data, 'post created', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function getPosts(req, res, next) {
  const { page, limit, active } = req.query;
  try {
    const result = await PostService.getPosts({ page, limit, active });
    !result.error
      ? success(req, res, result.data, 'posts listed', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function updatePost(req, res, next) {
  const post = req.body;
  const { id } = req.params;
  try {
    const result = await PostService.updatePost({ id, post });
    !result.error
      ? success(req, res, result.data, 'post updated', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function getPost(req, res, next) {
  const { slug } = req.params;
  try {
    const result = await PostService.getPost({ slug });
    !result.error
      ? success(req, res, result.data, 'post found', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function deletePost(req, res, next) {
  const { id } = req.params;
  try {
    const result = await PostService.deletePost(id);
    !result.error
      ? success(req, res, result.data, 'post deleted', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function getAllPosts(req, res, next) {
  const { page, limit } = req.query;
  try {
    const result = await PostService.getAllPosts({ page, limit });
    !result.error
      ? success(req, res, result.data, 'posts listed', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function publishPost(req, res, next) {
  const post = req.body;
  const { id } = req.params;
  try {
    const result = await PostService.updatePost({ id, post });
    const message = post.active ? 'post published' : 'post in review';
    !result.error
      ? success(req, res, result.data, message, 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

module.exports = {
  addPost,
  getPosts,
  updatePost,
  getPost,
  deletePost,
  getAllPosts,
  publishPost,
};
