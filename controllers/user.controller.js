const { UserService } = require("../services");
const { success, error, file } = require("../network/response");

async function getUsers(req, res, next) {
  try {
    const result = await UserService.getUsers();
    !result.error
      ? success(req, res, result.data, "user listed", 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}
async function getUsersActive(req, res, next) {
  const { active } = req.query;
  try {
    const result = await UserService.getUsersActive({ active });
    const message =
      active === "true" ? "user active listed" : "user in active listed";
    !result.error
      ? success(req, res, result.data, message, 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function uploadAvatar(req, res, next) {
  const { id } = req.params;
  const avatar = req.files.avatar;
  try {
    const result = await UserService.uploadAvatar({ id, avatar });
    !result.error
      ? success(req, res, result.data, "upload avatar sucess", 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function getAvatar(req, res, next) {
  const { avatar } = req.params;
  try {
    const result = await UserService.getAvatar({ avatar });
    !result.error
      ? file(req, res, result.data, 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function updateUser(req, res, next) {
  const { id } = req.params;
  const user = req.body;
  try {
    const result = await UserService.updateUser({ id, user });
    !result.error
      ? success(req, res, result.data, "updated user", 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function activateUser(req, res, next) {
  const { id } = req.params;
  const { active } = req.body;
  try {
    const result = await UserService.activateUser({ id, active });
    const message = active
      ? "user successfully activated"
      : "user successfully disabled ";
    !result.error
      ? success(req, res, result.data, message, 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function deleteUser(req, res, next) {
  const { id } = req.params;
  try {
    const result = await UserService.deleteUser({ id });
    !result.error
      ? success(req, res, result.data, "user deleted", 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function signUpAdmin(req, res, next) {
  const { body: user } = req;
  try {
    const result = await UserService.signUpAdmin({ user });
    !result.error
      ? success(req, res, result.data, "user created", 201)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

module.exports = {
  getUsers,
  getUsersActive,
  uploadAvatar,
  getAvatar,
  updateUser,
  activateUser,
  deleteUser,
  signUpAdmin
};
