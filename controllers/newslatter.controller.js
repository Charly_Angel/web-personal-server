const { NewsletterService } = require("../services");
const { success, error } = require("../network/response");

async function suscribeEmail(req, res, next) {
  const { email } = req.params;
  try {
    const result = await NewsletterService.suscribeEmail({ email });
    !result.error
      ? success(req, res, result.data, "email registered", 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

module.exports = {
  suscribeEmail
};
