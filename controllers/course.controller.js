const { CourseService } = require('../services');
const { success, error, file } = require('../network/response');

async function addCourse(req, res, next) {
  const course = req.body;
  try {
    const result = await CourseService.addCourse({ course });
    !result.error
      ? success(req, res, result.data, 'course registered', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function getCourses(req, res, next) {
  try {
    const result = await CourseService.getCourses();
    !result.error
      ? success(req, res, result.data, 'course listed', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function updateCourse(req, res, next) {
  const course = req.body;
  const { id } = req.params;
  try {
    const result = await CourseService.updateCourse({ id, course });
    !result.error
      ? success(req, res, result.data, 'course updated', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function deleteCourse(req, res, next) {
  const { id } = req.params;
  try {
    const result = await CourseService.deleteCourse({ id });
    !result.error
      ? success(req, res, result.data, 'course deleted', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function uploadImage(req, res, next) {
  const { id } = req.params;
  const image = req.files.image;
  try {
    const result = await CourseService.uploadImage({ id, image });
    !result.error
      ? success(req, res, result.data, 'upload image course success', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function getImageCourse(req, res, next) {
  const { image } = req.params;
  try {
    const result = await CourseService.getImageCourse({ image });
    !result.error
      ? file(req, res, result.data, 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

async function getCoursesOnOffer(req, res, next) {
  const { limit } = req.params;
  try {
    const result = await CourseService.getCoursesOnOffer(limit);
    !result.error
      ? success(req, res, result.data, 'courses on offer', 200)
      : error(req, res, result.boom);
  } catch (error) {
    next(error);
  }
}

module.exports = {
  addCourse,
  getCourses,
  updateCourse,
  deleteCourse,
  uploadImage,
  getImageCourse,
  getCoursesOnOffer,
};
