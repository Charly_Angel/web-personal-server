const { mongo } = require('../config');
const mongoose = require('mongoose');
const debug = require('debug')('app:db');

const connectionDB = async () => {
  try {
    mongoose.connect(mongo.uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
    debug(`success: database connected`);
  } catch (error) {
    debug(`error: database disconnected`);
  }
};

module.exports = connectionDB;
