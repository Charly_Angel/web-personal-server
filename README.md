# Web personal server ![Status badge](https://img.shields.io/badge/status-in%20progress-yellow)

API Rest para la gestión administrativa de una pagina web personal.

## Instalación
1. Clona este proyecto.
2. Ve a la carpeta del proyecto `cd web-personal-server`
3. Crear las variables de entorno del archivo `.env.example` y renombrar el archivo a `.env` para el ambiente local.
4. Instala las dependencias ejecutando el comando `yarn install`
5. Corre el ambiente local ejecutando el comando `yarn dev`

## License
The MIT License (MIT)
