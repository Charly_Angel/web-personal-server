const { Schema, model } = require('mongoose');

const Review = new Schema({
  name: {
    type: String,
    require: true,
  },
  jobTitle: {
    type: String,
    require: true,
  },
  review: {
    type: String,
    require: true,
  },
  image: {
    type: String,
    require: false,
  },
});

module.exports = model('Review', Review);
