const { date } = require("joi");
const { Schema, model } = require("mongoose");
const paginate = require("mongoose-paginate");
const PostSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  slug: {
    type: String,
    unique: true,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now()
  },
  active: {
    type: Boolean,
    default: false
  }
});

PostSchema.plugin(paginate);

module.exports = model("Post", PostSchema);
