const { Schema, model } = require('mongoose');

const CardInformation = new Schema({
  title: {
    type: String,
    require: true,
  },
  description: {
    type: String,
    require: true,
  },
  icon: {
    type: String,
    require: false,
  },
});

module.exports = model('CardInformation', CardInformation);
