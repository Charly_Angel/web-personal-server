const { Schema, model } = require("mongoose");

const NewsletterSchema = new Schema({
  email: {
    type: String,
    unique: true,
    required: true
  }
});

module.exports = model("Newsletter", NewsletterSchema);
