const { Schema, model } = require('mongoose');

const CourseSchema = new Schema({
  title: {
    type: String,
    require: true,
  },
  description: {
    type: String,
    require: false,
  },
  type: {
    type: String,
    require: false,
  },
  image: {
    type: String,
    require: false,
  },
  offer: {
    type: Boolean,
    require: false,
    default: false,
  },
  link: {
    type: String,
    require: true,
  },
  coupon: {
    type: String,
    require: false,
  },
  free: {
    type: Boolean,
    default: false,
    require: false,
  },
});

module.exports = model('Course', CourseSchema);
